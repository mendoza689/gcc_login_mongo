var express = require('express');
var path = require('path');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var loginRouter = require('./routes/login');
var app = express();
var mongoose=require('mongoose');


require('dotenv').config()




console.log(process.env.PORT)
console.log(process.env.DB_URI)

mongoose.set('useCreateIndex', true);
mongoose.connect(process.env.DB_URI,{ useNewUrlParser: true })
	.then(() => { 
		console.log('corriendo Base de datos :\x1b[32m%s\x1b[0m', 'online');
	}).
	catch((err) => {
	  throw err;
  });


// view engine setup

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));


app.listen(process.env.PORT || 3000, () => {
    console.log(`Server  app running on port: ${process.env.port}`);
});



//RUTAS
app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/login', loginRouter);



function stop() {
  app.close();
}

module.exports = app;
