var mongoose = require('mongoose');
var assert = require('assert');
var Usuario = require('../models/usuario');

const app = require('../app.js');
const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

describe('Validacion del modelo Usuario',  (done) => {
	
	before(function() {
		var usuario = new Usuario({
		nombre: 'test689',
		email: 'test689@gmail.com',
		password: 'test689',
	  });

		usuario.save().then(function() {
		}).catch(function() {
		});
	});
	
	it('Validando los campos del modelo:', (done) => {
    let user = new Usuario({ name: undefined });
    let validationResult = user.validateSync();


    assert(validationResult.errors.nombre.message === "El nombre es necesario");
	assert(validationResult.errors.password.message === "El password es necesario");
	assert(validationResult.errors.email.message === "El correo es necesario");
	done()
  });
	
	
	
	
	
	
	describe('Obteniendo el render login', (done) => {
		it('Returns a 200 response: accedió al render html del login', (done) => {
			chai.request(app)
				.get('/')
				.end((error, response) => {
					if (error) done(error);
					expect(response).to.have.status(200);
					done();
			});
		});
	});
	
	describe('Accediendo a la tabla Usuario de mongoDB', (done) => {
		it('Returns a 200: se accedió a la base de datos', (done) => {
			chai.request(app)
				.get('/usuarios')
				.end((error, response) => {
					expect(response).to.have.status(200);
					done();
			});
		});
	});
	
	
	describe('Accediendo una peticion de inicio de sesion', (done) => {
		it('Returns status  200 :credenciales correctas', (done) => {
			chai.request(app)
				.post('/login')
				.send({ email: 'test689@gmail.com', password: 'test689' })
				.end((error, response) => {
					expect(response).to.have.status(200);
					done();
			});
		});
	});
	
	describe('Accediendo una peticion de inicio de sesion', (done) => {
		it('Returns status 400: credenciales incorrectas', (done) => {
			chai.request(app)
				.post('/login')
				.send({ email: '@@@@@@@@@test689@gmail.com', password: 'test689' })
				.end((error, response) => {
					expect(response).to.have.status(400);
					done();
			});
		});
	});
});

