*12/5/2019*


## Trabajo practico GCC

### Aplicación web + Integración continua
___________________________________________
Se ha configurado tres  entornos:

* Desarrollo: utilizado para agregar funcionalidades a la aplicación (local)

* Homologación: utilizado para ejecutar las pruebas unitarias definidas. en caso de pasar las pruebas,  se iniciará el proceso de despliegue de la aplicación (git)

               
* Producción : utilizado para los  usuarios finales puedan utilizar la 
               aplicación (Heroku).
  


### Metodología
___________________________________________


* Sistema de Base de datos MongoDB: se creó un cluster , con bases de datos para los diferentes entornos

* Express.js: utilizado para desarrollar la aplicacion web

* Mocha y Chai: para crear las pruebas unitarias


* Gitlab: se utilizó para subir la aplicación en un repositorio, además se configuró la integración continua.




____________________________________________________________

### Integración Continua (CI)
____________________________________________________________
para utilizar  CI se creó el archivo ".gitlab-ci.yml", donde se especifican los diferentes stages, también se especifica la "image" a utilizar en este caso "node:latest".

 -Se definieron los siguentes stages:

      * build: se instalan las dependencias de node
      * test: se ejecutan las pruebas unitarias
      * deploy: se despliega en heroku.

finalmente, si no ha ocurrido ningún error , la  aplicación  estará desplegada en el entorno de producción 


[https://dashboard.heroku.com/apps/gccic22019](https://dashboard.heroku.com/apps/gccic22019)

