var express = require('express');
var router = express.Router();
//importar el modelo
var Usuario = require('../models/usuario');


router.post('/',(req, res) => {
    var body = req.body;
	
    Usuario.findOne({email:body.email}, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'error al buscar usuario',
                errors: err
            });
        }

        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrecta-email',
                errors: err
            });
        }

        if (!(body.password==usuarioDB.password)) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrecta-password',
                errors: err
            });
			
        }
		
        res.render('welcome', { title: 'Bienvenido a Fallout 4' });
    })
});

module.exports = router;
