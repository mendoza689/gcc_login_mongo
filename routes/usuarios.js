var express = require('express');
var router = express.Router();
//importar el modelo
var Usuario = require('../models/usuario');

/* GET una lista de todos los usuarios*/
router.get('/', function (req, res, next) {
  Usuario.find({}, 'nombre email ').exec(
    (err, usuario) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          mensaje: 'Error cargando usuario',
          error: ers
        });
      }
      Usuario.countDocuments({}, (err, count) => {
        res.status(200).json({
          ok: true,
          usuarios: usuario,
          total: count
        })

      });
    });
});

/* Post crear un usuario*/
router.post('/', (req, res) => {
  var body = req.body;
  var usuario = new Usuario({
    nombre: body.nombre,
    email: body.email,
    password: body.password,
    role: body.role,
  });

  usuario.save((err, user_save) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        mensaje: 'error al crear usuario',
        errors: err
      });
    }
    res.status(201).json({
      ok: true,
      usuario: user_save
    });
  });
});

module.exports = router;
